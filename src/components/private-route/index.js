import React from 'react';
import {useAuth} from "../../services/firebase";
import Route from 'react-router-dom/Route';
import Redirect from 'react-router-dom/Redirect';

export const PrivateRoute = ({children, ...rest}) => {
    const [authUser] = useAuth();

    return authUser ? (<Route {...rest}>{children}</Route>) : (<Redirect to={{
        pathname: '/login'
    }}/>)
}
