import React, {useState} from "react";
import {MenuBar} from "../menu-bar";
import {MainDrawer} from "../drawer";
import MailIcon from '@material-ui/icons/Mail';

export const MainContainer = ({children}) => {
    const [drawer, setDrawer] = useState(false);

    return (
        <>
            <MenuBar toggle={() => setDrawer(!drawer)} />
            <MainDrawer drawer={drawer} toggle={() => setDrawer(false)} links={[
                {
                    type: "link",
                    text: "test",
                    icon: <MailIcon />,
                    link: '/test'
                },
                {
                    type: "divider"
                },
                {
                    type: 'link',
                    text: 'test2',
                    icon: <MailIcon />,
                    link: '/test2'
                }
            ]} />
            {children}
        </>
    )
}
