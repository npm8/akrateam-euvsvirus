import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles({
    root: {
        width: 300,
    }
})

export const MainDrawer = ({drawer, toggle, links}) => {
    const {root} = useStyles()

    return (
        <Drawer anchor="left" open={drawer} onClose={toggle}>
            <List className={root}>
                {links.map(({type, link, text, icon}, index) => type === 'divider' ? <Divider /> : <ListItem button key={index}>
                    <ListItemIcon> {icon}</ListItemIcon>
                    <ListItemText primary={text}/>
                </ListItem>)}
            </List>
        </Drawer>
    );
}

