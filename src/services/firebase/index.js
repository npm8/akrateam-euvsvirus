import * as firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';
import {useState} from "react";

const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    appId: process.env.REACT_APP_ID,
}

// Firebase initialization.
firebase.initializeApp(firebaseConfig);

/**
 * Database object.
 * @type {firebase.database.Database}
 */
export const database = firebase.database();

/**
 * Authentication object
 * @type {firebase.auth.Auth}
 */
export const authentication = firebase.auth();

/**
 * Authorization hook.
 * @return {[firebase.User, function (void): Promise<*|firebase.auth.UserCredential>, function (void): Promise<boolean>]}
 */
export const useAuth = () => {
    const [authUser, setUser] = useState(null);

    authentication.onAuthStateChanged((user) => setUser(user))
    /**
     * Login with Google OAuth using Firebase Auth.
     * @return {Promise<*|firebase.auth.UserCredential>}
     */
    const login = async () => {
        try {
            return await authentication.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        } catch (authError) {
            return authError
        }
    }

    /**
     * Log out from Google OAuth using Firebase Auth.
     * @return {Promise<boolean>}
     */
    const logout = async () => {
        try {
            await authentication.signOut();
            return true
        } catch (authError) {
            return false
        }
    }

    return [authUser, login, logout]
}
