import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import {LoginPage} from "../login";
import {PrivateRoute} from "../../components/private-route";
import {MainContainer} from "../../components/main-container";

function App() {
  return (
      <BrowserRouter>
          <Route exac path="/login">
              <LoginPage />
          </Route>
          <PrivateRoute path="/">
              <MainContainer>
                  <Switch></Switch>
              </MainContainer>
          </PrivateRoute>
      </BrowserRouter>
  );
}

export default App;
