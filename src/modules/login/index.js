import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import {useAuth} from "../../services/firebase";
import Redirect from 'react-router-dom/Redirect';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    root: {
        height: "100vh"
    }
})

export const LoginPage = () => {
    const {root} = useStyles()

    const [user, login] = useAuth()

    return !user ? (
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            className={root}
        >
            <Button variant="contained" color="primary" size="large" onClick={() => login()}>
                Login with Google
            </Button>
        </Grid>
    ): <Redirect to={{
        pathname: '/'
    }}/>;
}

